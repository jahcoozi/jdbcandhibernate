package jdbc.hibernate;

public class User {


   private int iduser;
   private String name;
   private String adress;
   private int age;

   public User( String name, int age, String adress) {

       this.name = name;
       this.adress = adress;
       this.age = age;
   }

    public int getIduser() {
        return iduser;
    }

    public User setIduser(int iduser) {
        this.iduser = iduser;
        return this;
    }

    public String getName() {
        return name;
    }

    public User setName(String name) {
        this.name = name;
        return this;
    }

    public String getAdress() {
        return adress;
    }

    public User setAdress(String adress) {
        this.adress = adress;
        return this;
    }

    public int getAge() {
        return age;
    }

    public User setAge(int age) {
        this.age = age;
        return this;
    }
}
