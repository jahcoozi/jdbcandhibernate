package jdbc.hibernate;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class App {
    public static void main(String[] args) {

        UsersDao userDao = new UsersDao(initConnection());
        userDao.createUser(new User("Ion", 24, "Scortarilor"));
       // userDao.deleteUser(1);


    }
    static Connection initConnection() {
        String url = "jdbc:mysql://localhost:3306/shop?serverTimezone=UTC";
        String user = "root";
        String pass = "dt0344clseX!";
        Connection conn;
        try {
             conn = DriverManager.getConnection(url, user, pass);
        } catch (SQLException e) {
            e.printStackTrace();
            conn = null;
        }
        return conn;
    }

}
