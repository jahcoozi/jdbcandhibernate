package jdbc.hibernate;

import com.google.protobuf.BoolValueOrBuilder;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

public class UsersDao {

    Connection conn;

    public UsersDao(Connection conn) {
        this.conn = conn;
    }




     boolean createUser(User user) {

        try {
           Statement st = this.conn.createStatement();
         int result = st.executeUpdate("INSERT into users(name, age, adress) VALUES " +
                 "('" + user.getName() + "', '" + user.getAge() + "', '" + user.getAdress() + "')");

            return result == 1;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }


    }

    boolean deleteUser(int id) {
        try {
            Statement st = this.conn.createStatement();
            int result = st.executeUpdate("DELETE FROM users WHERE iduser = " + id);
            return result == 1;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

     boolean updateUser(User user) {
         try {
             Statement st = this.conn.createStatement();
           int result = st.executeUpdate("UPDATE users SET name = '" + user.setName("Marcel") + "', " +
                     "age = '" + user.setAge(24) + "', adress = '" + user.setAdress("Motilor") + "' WHERE iduser = " + user.getIduser());
           return result == 1;
         } catch (SQLException e) {
             e.printStackTrace();
         }
         return false;
     }


}
